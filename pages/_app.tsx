import "../styles/globals.css";
import type { AppProps } from "next/app";
import DbProvider from "../src/context/db.context";
import { Toaster } from "react-hot-toast";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <DbProvider>
      <div className="App">
        <Component {...pageProps} />
        <Toaster />
      </div>
    </DbProvider>
  );
}
