import { NextPage } from "next";
import Head from "next/head";
import { useEffect, useRef, useState } from "react";
import Header from "../src/components/Header/header";
import styles from "../styles/Home.module.css";
import { Line } from "react-chartjs-2";
import {
  Chart as ChartJS,
  CategoryScale,
  ChartData,
  LinearScale,
  LineController,
  LineElement,
  PointElement,
  Title,
  Filler,
  Chart,
  Legend,
} from "chart.js";
import { useDb } from "../src/context/db.context";
import { clearPreviewData } from "next/dist/server/api-utils";

const Home: NextPage = () => {
  const { getData, options, data, count, wells, getWells } = useDb();
  const [well, setWell] = useState(173);
  const [position, setPosition] = useState(0);
  const Refas = useRef<Chart<"line">>(null);

  ChartJS.register(
    CategoryScale,
    LinearScale,
    LineController,
    LineElement,
    PointElement,
    Title,
    Filler,
    Legend
  );

  useEffect(() => {
    const a = setTimeout(async () => {
      if (wells.length === 0) await getWells();
      if (position > count) {
        getData(well, 0);
        setPosition(0);
      } else {
        getData(well, position + 1);
        setPosition((x) => x + 1);
      }
    }, 1000);
    return () => clearTimeout(a);
  }, [position]);

  return (
    <main className="h-screen w-screen flex flex-col items-center">
      <Head>
        <title>Удалённый мониторинг</title>
        <meta name="description" content="Тут должно быть описание" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <div className={styles.container}>
        <div className={styles.graph}>
          <Line ref={Refas} data={data} options={options}></Line>
        </div>
        {wells && (
          <div className={styles.settings}>
            <div className={styles.settingsButtons}>
              <p className={styles.settingsTitle}>Доступные скважины</p>
              {wells.flatMap((element) => {
                return (
                  <button
                    key={element.id}
                    className={styles.button}
                    onClick={() => {
                      setWell(element.id);
                      setPosition(0);
                      Refas.current?.data.labels?.pop();
                      Refas.current?.data.datasets.forEach((dataset) => {
                        dataset.data.splice(0, dataset.data.length);
                      });
                      Refas.current?.update();
                    }}
                  >
                    {element.name}
                  </button>
                );
              })}
            </div>
          </div>
        )}
      </div>
    </main>
  );
};

export default Home;
