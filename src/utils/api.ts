import axios from 'axios';

export const apiPublic = axios.create({
    baseURL: 'https://gazprompractic.yami.town/api/v1'
});