export interface wellsTypes {
    id: number,
    name: string,
    to_control: number,
    set_control_date: Date,
    user_id: number,
}