export interface getDataTypes {
    data: {
        id: number;
        time: Date;
        P_ca: number;
        Q_ca: number;
        P_tr: number;
        P_zatr: number;
        P_dikt: number;
        T_izm: number;
        operating_mode_P_tr: number;
        operating_mode_P_zatr: number;
        diff_T_izm: number;
        origin_P_zatr: number;
        closed: number;
        purging: number;
        gas_hydrate: number;
        liquid_release_hydrate: number;
        liquid_release: number;
        big_liquid_release: number;
        closed_time: number;
        work_time: number;
        delay_before_operating_mode: number;
        attention_time: number;
        well_bore_id: {
            id: number;
            name: string;
            well_id: number;
            status_id: number;
            to_control: number;
            set_control_date: Date;
            user_id: number;
        }
    };
    count: number;
}