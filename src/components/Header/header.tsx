import { NextPage } from "next";
import Image from "next/image";
import { useRouter } from "next/router";
import logotype from "../../../public/logotype.png";
import styles from "./css/Header.module.css";
import { AiOutlineReload } from "react-icons/ai";

const Header: NextPage = () => {
  const router = useRouter();
  return (
    <header className={styles.header}>
      <div className={styles.header__main}>
        <div className={styles.header__left}>
          <Image src={logotype} alt={"logo"}></Image>
        </div>
        <div className={styles.header__center}>
          <p>Удалённый мониторинг</p>
        </div>
        <div className={styles.header__right}>
          <button onClick={router.reload}>
            <AiOutlineReload className="dark:text-black text-white" />
          </button>
        </div>
      </div>
    </header>
  );
};

export default Header;
