import {
  createContext,
  FC,
  PropsWithChildren,
  useCallback,
  useContext,
  useState,
} from "react";
import { DbService } from "../services/db.service";
import { wellsTypes } from "../types/wellsTypes";

type DbContextData = {
  getData: (wellId: number, position: number) => void;
  getWells: () => Promise<boolean>;
  options: any;
  data: any;
  count: number;
  wells: wellsTypes[];
};

export const DbContext = createContext({} as DbContextData);

export const useDb = () => useContext(DbContext);

const DbProvider: FC<PropsWithChildren> = ({ children }) => {
  const [count, setCount] = useState(0);

  const options = {
    animation: false,

    elements: {
      line: {
        tension: 0,
        borderWidth: 2,
        borderColor: "#fff",
        fill: "start",
        backgroundColor: "#000",
      },
      point: {
        radius: 0,
        hitRadius: 10,
      },
    },
    scales: {
      y: {
        type: "linear" as const,
        display: true,
        position: "left" as const,
      },
      y1: {
        type: "linear" as const,
        display: true,
        position: "right" as const,
        beginAtZero: true,
        min: 0,
        max: 5,
        ticks: {
          stepSize: 0.5,
        },
      },
      y2: {
        type: "linear" as const,
        display: true,
        position: "right" as const,
        beginAtZero: true,
        min: 0,
        max: 25,
        ticks: {
          stepSize: 5,
        },
      },
    },
    plugins: {
      legend: {
        display: true,
        position: "bottom",
        fillStyle: "#1A1C1E",
      },
      filler: {
        propagate: false,
      },
    },
  };

  const [wells, setWells] = useState<wellsTypes[]>([]);

  const [data, setData] = useState<any>({
    datasets: [
      {
        label: "P_ca",
        data: [],
        fill: false,
        borderColor: "#FF8C00",
      },
      {
        label: "Q_ca",
        data: [],
        fill: false,
        borderColor: "#4B0082",
        yAxisID: "y1",
      },
      {
        label: "P_tr",
        data: [],
        fill: false,
        borderColor: "#FF0000",
      },
      {
        label: "origin_P_zatr",
        data: [],
        fill: false,
        borderColor: "#00008B",
      },
      {
        label: "P_dikt",
        data: [],
        fill: false,
        borderColor: "#90EE90",
      },
      {
        label: "T_izm",
        data: [],
        fill: false,
        borderColor: "#FFFF00",
        yAxisID: "y2",
      },
      {
        label: "closed",
        data: [],
        fill: true,
        borderColor: "#696969",
        backgroundColor: "rgba(128, 128, 128, 0.5)",
      },
      {
        label: "purging",
        data: [],
        fill: true,
        borderColor: "#D3D3D3",
        backgroundColor: "rgba(216, 216, 216, 0.5)",
      },
      {
        label: "gas_hydrate",
        data: [],
        fill: true,
        borderColor: "#ADD8E6",
        backgroundColor: "rgba(138, 180, 194, 0.5)",
      },
      {
        label: "liquid_release_hydrate",
        data: [],
        fill: true,
        borderColor: "#CD5C5C",
        backgroundColor: "rgba(225, 120, 120, 0.5)",
      },
      {
        label: "liquid_release",
        data: [],
        fill: true,
        borderColor: "#FF0000",
        backgroundColor: "rgba(255, 0, 0, 0.5)",
      },
      {
        label: "big_liquid_release",
        data: [],
        fill: true,
        borderColor: "#8B0000",
        backgroundColor: "rgba(175, 0, 0, 0.5)",
      },
    ],
  });

  const getWells = useCallback(async () => {
    const db = await DbService.getWells();
    if (db === null) return false;
    setWells(db);
    return true;
  }, [wells]);

  const getData = useCallback(
    async (wellId: number, position: number) => {
      const db = await DbService.getAllById(wellId, position);
      if (db != null) {
        setCount(db.count);
        setData(
          (x: {
            datasets: {
              data: any;
              label: { toString: () => string | number | symbol };
            }[];
          }) => ({
            datasets: x.datasets.map(
              (element: {
                data: any;
                label: { toString: () => string | number | symbol };
              }) => {
                return {
                  ...element,
                  data: [
                    ...element.data,
                    {
                      x: db.data[0].time,
                      y: db.data[0][
                        element.label?.toString() as keyof typeof db.data
                      ],
                    },
                  ],
                };
              }
            ),
          })
        );
      }
    },
    [count, data]
  );

  return (
    <DbContext.Provider
      value={{
        count,
        wells,
        getData,
        getWells,
        data,
        options,
      }}
    >
      {children}
    </DbContext.Provider>
  );
};

export default DbProvider;
