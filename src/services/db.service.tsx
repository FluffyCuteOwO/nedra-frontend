import { apiPublic } from "../utils/api";
import toast from "react-hot-toast";

export const DbService = {
  async getAllById(wellId: number, position: number) {
    const { data } = await apiPublic
      .get(`/result/${wellId}/${position}`, { timeout: 5000 })
      .catch((e) => {
        toast.error("Ошибка при загрузке данных: " + e);
        return {
          data: null,
        };
      });
    return data;
  },

  async getWells() {
    const { data } = await apiPublic
      .get(`/wells`, { timeout: 5000 })
      .catch((e) => {
        toast.error("Ошибка при загрузке данных: " + e);
        return {
          data: null,
        };
      });
    return data;
  },
};
